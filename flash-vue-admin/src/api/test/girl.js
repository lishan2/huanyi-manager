import request from '@/utils/request'

export function getList(params) {
    return request({
        url: '/test/girl/list',
        method: 'get',
        params
    })
}


export function save(params) {
    return request({
        url: '/test/girl',
        method: 'post',
        params
    })
}

export function remove(id) {
    return request({
        url: '/test/girl',
        method: 'delete',
        params: {
            id: id
        }
    })
}
