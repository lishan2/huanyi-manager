import request from '@/utils/request'

export function getList(params) {
    return request({
        url: '/business/letter/list',
        method: 'get',
        params
    })
}


export function save(params) {
    return request({
        url: '/business/letter',
        method: 'post',
        params
    })
}

export function remove(id) {
    return request({
        url: '/business/letter',
        method: 'delete',
        params: {
            id: id
        }
    })
}
