import request from '@/utils/request'

export function getList(params) {
    return request({
        url: '/business/image/list',
        method: 'get',
        params
    })
}


export function save(params) {
    return request({
        url: '/business/image',
        method: 'post',
        params
    })
}

export function remove(id) {
    return request({
        url: '/business/image',
        method: 'delete',
        params: {
            id: id
        }
    })
}

export function get(id) {
    return request({
        url: '/business/image',
        method: 'get',
        params: {
            id: id
        }
    })
}
