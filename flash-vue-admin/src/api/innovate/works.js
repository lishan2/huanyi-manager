import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/innovate/works/list',
    method: 'get',
    params
  })
}


export function save(params) {
  return request({
    url: '/innovate/works',
    method: 'post',
    data : params
  })
}

export function remove(id) {
  return request({
    url: '/innovate/works',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function get(id) {
  return request({
    url: '/innovate/works',
    method: 'get',
    params: {
      id: id
    }
  })
}
