import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/innovate/practice/list',
    method: 'get',
    params
  })
}


export function save(params) {
  return request({
    url: '/innovate/practice',
    method: 'post',
    data : params
  })
}

export function remove(id) {
  return request({
    url: '/innovate/practice',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function get(id) {
  return request({
    url: '/innovate/practice',
    method: 'get',
    params: {
      id: id
    }
  })
}
