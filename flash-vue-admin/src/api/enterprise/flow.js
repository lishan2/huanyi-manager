import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/enterprise/flow/list',
    method: 'get',
    params
  })
}


export function save(params) {
  return request({
    url: '/enterprise/flow',
    method: 'post',
    data : params
  })
}

export function remove(id) {
  return request({
    url: '/enterprise/flow',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function get(id) {
  return request({
    url: '/enterprise/flow',
    method: 'get',
    params: {
      id: id
    }
  })
}

export function details() {
  return request({
    url: '/enterprise/flow/details',
    method: 'get'
  })
}
