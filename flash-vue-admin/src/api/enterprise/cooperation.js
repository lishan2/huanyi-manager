import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/enterprise/cooperation/list',
    method: 'get',
    params
  })
}


export function save(params) {
  return request({
    url: '/enterprise/cooperation',
    method: 'post',
    data : params
  })
}

export function remove(id) {
  return request({
    url: '/enterprise/cooperation',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function get(id) {
  return request({
    url: '/enterprise/cooperation',
    method: 'get',
    params: {
      id: id
    }
  })
}
