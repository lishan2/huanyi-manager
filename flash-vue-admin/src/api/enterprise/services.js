import request from '@/utils/request'

export function getList(params) {
  return request({
    url: '/enterprise/services/list',
    method: 'get',
    params
  })
}


export function save(params) {
  return request({
    url: '/enterprise/services',
    method: 'post',
    data : params
  })
}

export function remove(id) {
  return request({
    url: '/enterprise/services',
    method: 'delete',
    params: {
      id: id
    }
  })
}

export function get(id) {
  return request({
    url: '/enterprise/services',
    method: 'get',
    params: {
      id: id
    }
  })
}
