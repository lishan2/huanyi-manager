import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'             the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: 'dashboard', icon: 'dashboard', affix: true }
    }]
  },
  //轮播图管理
  {
    path: '/banner',
    component: Layout,
    // redirect: '/banner',
    children: [{
      path: 'banner',
      name: 'Banner',
      component: () => import('@/views/business/banner/index'),
      meta: { title: 'banner', icon: 'dashboard', affix: false }
    }]
  },

  {
    path: '/innovate', //创新
    component: Layout,
    // redirect: '/innovate',
    meta: { title: 'innovate', icon: 'dashboard', affix: false },
    children: [{
      path: 'works',  //学生作品
      name: '学生作品',
      component: () => import('@/views/innovate/works/index'),
      meta: { title: '学生作品', icon: 'dashboard', affix: false }
    },
    {
      path: 'education',//联合教育
      name: '联合教育',
      component: () => import('@/views/innovate/education/index'),
      meta: { title: '联合教育', icon: 'dashboard', affix: false }
    },
    {
      path: 'practice',  //实践成果
      name: '实践成果',
      component: () => import('@/views/innovate/practice/index'),
      meta: { title: '实践成果', icon: 'dashboard', affix: false }
    }]
  },
  {
    path: '/enterprise', //创业
    component: Layout,
    redirect: '/enterprise',
    meta: { title: 'enterprise', icon: 'dashboard', affix: false },
    children: [{
      path: '/enterprise/flowEdit',  //创业流程   /enterprise/flowEdit
      name: '创业流程',
      component: () => import('@/views/enterprise/flow/edit.vue'),
      meta: { title: '创业流程', icon: 'dashboard', affix: false }
    },{
      path: 'cooperation',  //企业合作
      name: '企业合作',
      component: () => import('@/views/enterprise/cooperation/index'),
      meta: { title: '企业合作', icon: 'dashboard', affix: false }
    },{
      path: 'services',  //地方服务
      name: '地方服务',
      component: () => import('@/views/enterprise/services/index'),
      meta: { title: '地方服务', icon: 'dashboard', affix: false }
    }]
  },
  {
    path: '/letter',  //私信我们
    component: Layout,
    // redirect: '/letter',
    children: [{
      path: 'letter',
      name: '私信我们',
      component: () => import('@/views/business/letter/index'),
      meta: { title: '私信我们', icon: 'dashboard', affix: false }
    }]
  },
  {
    path: '/affiche',  //公告信息
    component: Layout,
    redirect: '/affiche',
    children: [{
      path: 'affiche',
      name: '公告信息',
      component: () => import('@/views/enterprise/affiche/index'),
      meta: { title: '公告信息', icon: 'dashboard', affix: false }
    }]
  },
  {
    path: '/account',
    component: Layout,
    hidden: true,
    redirect: 'noredirect',
    children: [
      {
        path: 'profile',
        name: '个人资料',
        component: () => import('@/views/account/profile.vue'),

        meta: { title: '个人资料' }

      },
      {
        path: 'timeline',
        name: '最近活动',
        component: () => import('@/views/account/timeline.vue'),
        hidden: true,
        meta: { title: '最近活动' }

      },
      {
        path: 'updatePwd',
        name: '修改密码',
        component: () => import('@/views/account/updatePwd.vue'),
        hidden: true,
        meta: { title: '修改密码' }
      }
    ]
  }

]

const createRouter = () => new Router({
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
