package cn.enilu.flash.api.controller.enterprise;

import cn.enilu.flash.api.controller.BaseController;
import cn.enilu.flash.bean.constant.factory.PageFactory;
import cn.enilu.flash.bean.core.BussinessLog;
import cn.enilu.flash.bean.entity.enterprise.Affiche;
import cn.enilu.flash.bean.entity.enterprise.Cooperation;
import cn.enilu.flash.bean.entity.enterprise.Flow;
import cn.enilu.flash.bean.enumeration.Permission;
import cn.enilu.flash.bean.vo.front.Rets;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.service.enterprise.AfficheService;
import cn.enilu.flash.service.enterprise.FlowService;
import cn.enilu.flash.utils.DateUtil;
import cn.enilu.flash.utils.factory.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 公告管理
 */
@RestController
@RequestMapping("/enterprise/affiche")
public class AfficheController extends BaseController {

    @Autowired
    private AfficheService afficheService;

    @RequestMapping(method = RequestMethod.POST)
    @BussinessLog(value = "编辑文章",key="name")
    @RequiresPermissions(value = {Permission.ARTICLE_EDIT})
    public Object save(){
        Affiche affiche = getFromJson(Affiche.class);
        if(affiche.getId()!=null){
            Affiche old = afficheService.get(affiche.getId());
            old.setAuthor(affiche.getAuthor());
            old.setContent(affiche.getContent());
            old.setIdChannel(affiche.getIdChannel());
            old.setImg(affiche.getImg());
            old.setTitle(affiche.getTitle());
            afficheService.update(old);
        }else {
            afficheService.insert(affiche);
        }
        return Rets.success();
    }
    @RequestMapping(method = RequestMethod.DELETE)
    @BussinessLog(value = "删除文章",key="id")
    @RequiresPermissions(value = {Permission.ARTICLE_DEL})
    public Object remove(Long id){
        afficheService.delete(id);
        return Rets.success();
    }
    @RequestMapping(method = RequestMethod.GET)
    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object get(@Param("id") Long id) {
        Affiche affiche = afficheService.get(id);
        return Rets.success(affiche);
    }
    @RequestMapping(value = "/list",method = RequestMethod.GET)
//    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object list(@RequestParam(required = false) String title,
                       @RequestParam(required = false) String author,
                       @RequestParam(required = false) String startDate,
                       @RequestParam(required = false) String endDate
                       ) {
        Page<Affiche> page = new PageFactory<Affiche>().defaultPage();
        page.addFilter("title", SearchFilter.Operator.LIKE,title);
        page.addFilter("author", SearchFilter.Operator.EQ,author);
        page.addFilter("createTime", SearchFilter.Operator.GTE, DateUtil.parse(startDate,"yyyyMMddHHmmss"));
        page.addFilter("createTime", SearchFilter.Operator.LTE, DateUtil.parse(endDate,"yyyyMMddHHmmss"));
        page = afficheService.queryPage(page);
        return Rets.success(page);
    }

    @RequestMapping(value = "/details",method = RequestMethod.GET)
    public Object details(@Param("id") Long id) {
        Affiche affiche = afficheService.get(id);
        return Rets.success(affiche);
    }
}
