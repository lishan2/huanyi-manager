package cn.enilu.flash.api.controller.enterprise;

import cn.enilu.flash.api.controller.BaseController;
import cn.enilu.flash.bean.constant.factory.PageFactory;
import cn.enilu.flash.bean.core.BussinessLog;
import cn.enilu.flash.bean.entity.enterprise.ServicesTable;
import cn.enilu.flash.bean.entity.innovate.Education;
import cn.enilu.flash.bean.enumeration.Permission;
import cn.enilu.flash.bean.vo.front.Rets;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.service.enterprise.ServicesTableService;
import cn.enilu.flash.utils.DateUtil;
import cn.enilu.flash.utils.factory.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 地方服务
 */
@RestController
@RequestMapping("/enterprise/services")
public class ServicesController extends BaseController {

    @Autowired
    private ServicesTableService servicesTableService;
    @RequestMapping(method = RequestMethod.POST)
    @BussinessLog(value = "编辑文章",key="name")
    @RequiresPermissions(value = {Permission.ARTICLE_EDIT})
    public Object save(){
        ServicesTable servicesTable = getFromJson(ServicesTable.class);
        if(servicesTable.getId()!=null){
            ServicesTable old = servicesTableService.get(servicesTable.getId());
            old.setAuthor(servicesTable.getAuthor());
            old.setContent(servicesTable.getContent());
            old.setIdChannel(servicesTable.getIdChannel());
            old.setImg(servicesTable.getImg());
            old.setTitle(servicesTable.getTitle());
            servicesTableService.update(old);
        }else {
            servicesTableService.insert(servicesTable);
        }
        return Rets.success();
    }
    @RequestMapping(method = RequestMethod.DELETE)
    @BussinessLog(value = "删除文章",key="id")
    @RequiresPermissions(value = {Permission.ARTICLE_DEL})
    public Object remove(Long id){
        servicesTableService.delete(id);
        return Rets.success();
    }
    @RequestMapping(method = RequestMethod.GET)
    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object get(@Param("id") Long id) {
        ServicesTable servicesTable = servicesTableService.get(id);
        return Rets.success(servicesTable);
    }
    @RequestMapping(value = "/list",method = RequestMethod.GET)
//    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object list(@RequestParam(required = false) String title,
                       @RequestParam(required = false) String author,
                       @RequestParam(required = false) String startDate,
                       @RequestParam(required = false) String endDate
                       ) {
        Page<ServicesTable> page = new PageFactory<ServicesTable>().defaultPage();
        page.addFilter("title", SearchFilter.Operator.LIKE,title);
        page.addFilter("author", SearchFilter.Operator.EQ,author);
        page.addFilter("createTime", SearchFilter.Operator.GTE, DateUtil.parse(startDate,"yyyyMMddHHmmss"));
        page.addFilter("createTime", SearchFilter.Operator.LTE, DateUtil.parse(endDate,"yyyyMMddHHmmss"));
        page = servicesTableService.queryPage(page);
        return Rets.success(page);
    }

    @RequestMapping(value = "/details",method = RequestMethod.GET)
    public Object details(@Param("id") Long id) {
        ServicesTable servicesTable = servicesTableService.get(id);
        return Rets.success(servicesTable);
    }
}
