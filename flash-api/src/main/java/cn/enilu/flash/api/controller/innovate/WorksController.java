package cn.enilu.flash.api.controller.innovate;

import cn.enilu.flash.api.controller.BaseController;
import cn.enilu.flash.bean.constant.factory.PageFactory;
import cn.enilu.flash.bean.core.BussinessLog;
import cn.enilu.flash.bean.entity.innovate.Works;
import cn.enilu.flash.bean.enumeration.Permission;
import cn.enilu.flash.bean.vo.front.Rets;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.service.innovate.WorksService;
import cn.enilu.flash.utils.DateUtil;
import cn.enilu.flash.utils.factory.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 学生作品
 */
@RestController
@RequestMapping("/innovate/works")
public class WorksController extends BaseController {

    @Autowired
    private WorksService worksService;
    @RequestMapping(method = RequestMethod.POST)
    @BussinessLog(value = "编辑文章",key="name")
    @RequiresPermissions(value = {Permission.ARTICLE_EDIT})
    public Object save(){
        Works works = getFromJson(Works.class);
        if(works.getId()!=null){
            Works old = worksService.get(works.getId());

            old.setAuthor(works.getAuthor());
            old.setContent(works.getContent());
            old.setIdChannel(works.getIdChannel());
            old.setImg(works.getImg());
            old.setTitle(works.getTitle());
            worksService.update(old);
        }else {
            worksService.insert(works);
        }
        return Rets.success();
    }
    @RequestMapping(method = RequestMethod.DELETE)
    @BussinessLog(value = "删除文章",key="id")
    @RequiresPermissions(value = {Permission.ARTICLE_DEL})
    public Object remove(Long id){
        worksService.delete(id);
        return Rets.success();
    }
    @RequestMapping(method = RequestMethod.GET)
    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object get(@Param("id") Long id) {
        Works article = worksService.get(id);
        return Rets.success(article);
    }
    @RequestMapping(value = "/list",method = RequestMethod.GET)
//    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object list(@RequestParam(required = false) String title,
                       @RequestParam(required = false) String author,
                       @RequestParam(required = false) String startDate,
                       @RequestParam(required = false) String endDate
                       ) {
        Page<Works> page = new PageFactory<Works>().defaultPage();
        page.addFilter("title", SearchFilter.Operator.LIKE,title);
        page.addFilter("author", SearchFilter.Operator.EQ,author);
        page.addFilter("createTime", SearchFilter.Operator.GTE, DateUtil.parse(startDate,"yyyyMMddHHmmss"));
        page.addFilter("createTime", SearchFilter.Operator.LTE, DateUtil.parse(endDate,"yyyyMMddHHmmss"));
        page = worksService.queryPage(page);
        return Rets.success(page);
    }

    @RequestMapping(value = "/details",method = RequestMethod.GET)
    public Object details(@Param("id") Long id) {
        Works article = worksService.get(id);
        return Rets.success(article);
    }

}
