package cn.enilu.flash.api.controller.business;

import cn.enilu.flash.api.controller.BaseController;
import cn.enilu.flash.bean.constant.factory.PageFactory;
import cn.enilu.flash.bean.core.BussinessLog;
import cn.enilu.flash.bean.entity.business.Image;
import cn.enilu.flash.bean.vo.front.Rets;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.service.business.ImageService;
import cn.enilu.flash.utils.DateUtil;
import cn.enilu.flash.utils.factory.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 联合教育
 */
@RestController
@RequestMapping("/business/image")
public class ImageController extends BaseController {
    @Autowired
    private ImageService imageService;

    @RequestMapping(method = RequestMethod.POST)
    @BussinessLog(value = "编辑", key = "title")
//    @RequiresPermissions(value = {Permission.CHANNEL_EDIT})
    public Object save(@ModelAttribute @Valid Image image) {
        String info = "";
        SearchFilter filter =  SearchFilter.build("status", 1);
        long num = imageService.count(filter);
        Long status = image.getStatus();
        if (num >= 6 && status ==1){
            return Rets.failure("已发布数据6条，请重新操作！");
        } else {
            if(image.getId()==null) {
                imageService.insert(image);
            }else{
                imageService.update(image);
            }
            return Rets.success();
        }
    }

    @RequestMapping(method = RequestMethod.GET)
//    @RequiresPermissions(value = {Permission.ARTICLE})
    public Object get(@Param("id") Long id) {
        Image image = imageService.get(id);
        return Rets.success(image);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @BussinessLog(value = "删除", key = "id")
//    @RequiresPermissions(value = {Permission.CHANNEL_DEL})
    public Object remove(Long id) {
        imageService.delete(id);
        return Rets.success();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Object list(@RequestParam(required = false) String type,
                       @RequestParam(required = false) String title,
                       @RequestParam(required = false) String author,
                       @RequestParam(required = false) String startDate,
                       @RequestParam(required = false) String endDate
                    ) {
//        List<Image> list = imageService.queryAll();
        Page<Image> page = new PageFactory<Image>().defaultPage();
        if (type.equals("web")){
            page.addFilter("status",1);
        }
        Sort sort = new Sort(Sort.Direction.DESC, "status");
        page.setSort(sort);
        page.addFilter("title", SearchFilter.Operator.LIKE,title);
        page.addFilter("author", SearchFilter.Operator.EQ,author);
        page.addFilter("createTime", SearchFilter.Operator.GTE, DateUtil.parse(startDate,"yyyyMMddHHmmss"));
        page.addFilter("createTime", SearchFilter.Operator.LTE, DateUtil.parse(endDate,"yyyyMMddHHmmss"));

        page = imageService.queryPage(page);
        return Rets.success(page);
    }
}
