package cn.enilu.flash.api.controller.business;

import cn.enilu.flash.api.controller.BaseController;
import cn.enilu.flash.bean.vo.front.Rets;
import cn.enilu.flash.service.business.ImageService;
import cn.enilu.flash.service.enterprise.AfficheService;
import cn.enilu.flash.service.enterprise.CooperationService;
import cn.enilu.flash.service.enterprise.FlowService;
import cn.enilu.flash.service.enterprise.ServicesTableService;
import cn.enilu.flash.service.innovate.EducationService;
import cn.enilu.flash.service.innovate.PracticeService;
import cn.enilu.flash.service.innovate.WorksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 联合教育
 */
@RestController
@RequestMapping("/business/data")
public class DataController extends BaseController {

    @Autowired
    private ImageService imageService;
    @Autowired
    private AfficheService afficheService;
    @Autowired
    private CooperationService cooperationService;
    @Autowired
    private FlowService flowService;
    @Autowired
    private ServicesTableService servicesTableService;
    @Autowired
    private EducationService educationService;
    @Autowired
    private PracticeService practiceService;
    @Autowired
    private WorksService worksService;


    @RequestMapping(value = "/details",method = RequestMethod.GET)
    public Object details(@Param("id") Long id,@Param("type") String type) {
        Object res = null;
        switch(type){
            case "banner": //轮播图详情
                res = imageService.get(id);
                break;
            case "works": //学生作品
                res = worksService.get(id);
                break;
            case "education": //联合教育
                res = educationService.get(id);
                break;
            case "practice": //实践成果
                res = practiceService.get(id);
                break;
            case "flow": //创业流程
                res = flowService.get(id);
                break;
            case "cooperation": //企业合作
                res = cooperationService.get(id);
                break;
            case "services": //地方服务
                res = servicesTableService.get(id);
                break;
            case "affiche": //公告管理
                res = afficheService.get(id);
                break;
            default:
                break;
        }
        return Rets.success(res);
    }
}
