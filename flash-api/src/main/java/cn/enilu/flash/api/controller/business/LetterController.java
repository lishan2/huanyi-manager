package cn.enilu.flash.api.controller.business;

import cn.enilu.flash.api.controller.BaseController;
import cn.enilu.flash.bean.constant.factory.PageFactory;
import cn.enilu.flash.bean.core.BussinessLog;
import cn.enilu.flash.bean.entity.business.Letter;
import cn.enilu.flash.bean.enumeration.Permission;
import cn.enilu.flash.bean.vo.front.Rets;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.service.business.LetterService;
import cn.enilu.flash.utils.DateUtil;
import cn.enilu.flash.utils.factory.Page;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * 企业合作
 */
@RestController
@RequestMapping("/business/letter")
public class LetterController extends BaseController {

    @Autowired
    private LetterService letterService;

//    @RequestMapping(method = RequestMethod.POST)
//    @BussinessLog(value = "编辑", key = "title")
//    @RequiresPermissions(value = {Permission.CHANNEL_EDIT})
    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public Object save(@ModelAttribute @Valid Letter letter) {
        if(letter.getId()==null) {
            letterService.insert(letter);
        }else{
            letterService.update(letter);
        }
        return Rets.success();
    }

    @RequestMapping(method = RequestMethod.DELETE)
    @BussinessLog(value = "删除", key = "id")
//    @RequiresPermissions(value = {Permission.CHANNEL_DEL})
    public Object remove(Long id) {
        letterService.delete(id);
        return Rets.success();
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public Object list(@RequestParam(required = false) String title,
                       @RequestParam(required = false) String content,
                       @RequestParam(required = false) String startDate,
                       @RequestParam(required = false) String endDate
                    ) {
        Page<Letter> page = new PageFactory<Letter>().defaultPage();
        page.addFilter("title", SearchFilter.Operator.LIKE,title);
        page.addFilter("content", SearchFilter.Operator.LIKE,content);
        page.addFilter("createTime", SearchFilter.Operator.GTE, DateUtil.parse(startDate,"yyyyMMddHHmmss"));
        page.addFilter("createTime", SearchFilter.Operator.LTE, DateUtil.parse(endDate,"yyyyMMddHHmmss"));

        page = letterService.queryPage(page);
        return Rets.success(page);
    }
}
