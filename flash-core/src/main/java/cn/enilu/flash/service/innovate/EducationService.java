package cn.enilu.flash.service.innovate;

import cn.enilu.flash.bean.entity.innovate.Education;
import cn.enilu.flash.bean.enumeration.cms.ChannelEnum;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.dao.innovate.EducationRepository;
import cn.enilu.flash.service.BaseService;
import cn.enilu.flash.utils.factory.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EducationService extends BaseService<Education, Long, EducationRepository> {
    @Autowired
    private EducationRepository educationRepository;

    /**
     * 查询首页最近5条资讯数据
     *
     * @return
     */
    public List<Education> queryIndexNews() {
        Page<Education> page = query(1, 5, ChannelEnum.NEWS.getId());
        return page.getRecords();
    }

    public Page<Education> query(int currentPage, int size, Long idChannel) {
        Page page = new Page(currentPage, size, "id");
        page.addFilter(SearchFilter.build("idChannel", SearchFilter.Operator.EQ, idChannel));
        return queryPage(page);

    }
}
