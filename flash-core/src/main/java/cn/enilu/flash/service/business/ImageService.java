package cn.enilu.flash.service.business;

import cn.enilu.flash.bean.entity.business.Image;
import cn.enilu.flash.dao.business.ImageRepository;
import cn.enilu.flash.service.BaseService;
import org.springframework.stereotype.Service;

/**
 * 文章频道service
 *
 * @author ：enilu
 * @date ：Created in 2019/6/30 13:06
 */
@Service
public class ImageService extends BaseService<Image,Long, ImageRepository> {
    //统计已发布数据个数

}
