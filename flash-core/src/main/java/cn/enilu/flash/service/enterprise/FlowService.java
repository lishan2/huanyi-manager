package cn.enilu.flash.service.enterprise;

import cn.enilu.flash.bean.entity.enterprise.Flow;
import cn.enilu.flash.bean.enumeration.cms.ChannelEnum;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.dao.enterprise.FlowRepository;
import cn.enilu.flash.service.BaseService;
import cn.enilu.flash.utils.factory.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FlowService extends BaseService<Flow, Long, FlowRepository> {
    @Autowired
    private FlowRepository flowRepository;

    /**
     * 查询首页最近5条资讯数据
     *
     * @return
     */
    public List<Flow> queryIndexNews() {
        Page<Flow> page = query(1, 5, ChannelEnum.NEWS.getId());
        return page.getRecords();
    }

    public Page<Flow> query(int currentPage, int size, Long idChannel) {
        Page page = new Page(currentPage, size, "id");
        page.addFilter(SearchFilter.build("idChannel", SearchFilter.Operator.EQ, idChannel));
        return queryPage(page);

    }

    public Flow findFirst(){
        String sql = "select * from t_business_flow limit 1;";
        Flow flow = flowRepository.get(sql);
        return flow;
    }
}
