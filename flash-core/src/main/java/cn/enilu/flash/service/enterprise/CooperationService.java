package cn.enilu.flash.service.enterprise;

import cn.enilu.flash.bean.entity.enterprise.Cooperation;
import cn.enilu.flash.bean.enumeration.cms.ChannelEnum;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.dao.enterprise.CooperationRepository;
import cn.enilu.flash.service.BaseService;
import cn.enilu.flash.utils.factory.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CooperationService extends BaseService<Cooperation, Long, CooperationRepository> {
    @Autowired
    private CooperationRepository cooperationRepository;

    /**
     * 查询首页最近5条资讯数据
     *
     * @return
     */
    public List<Cooperation> queryIndexNews() {
        Page<Cooperation> page = query(1, 5, ChannelEnum.NEWS.getId());
        return page.getRecords();
    }

    public Page<Cooperation> query(int currentPage, int size, Long idChannel) {
        Page page = new Page(currentPage, size, "id");
        page.addFilter(SearchFilter.build("idChannel", SearchFilter.Operator.EQ, idChannel));
        return queryPage(page);

    }
}
