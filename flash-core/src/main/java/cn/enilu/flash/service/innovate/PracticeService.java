package cn.enilu.flash.service.innovate;

import cn.enilu.flash.bean.entity.innovate.Practice;
import cn.enilu.flash.bean.enumeration.cms.ChannelEnum;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.dao.innovate.PracticeRepository;
import cn.enilu.flash.service.BaseService;
import cn.enilu.flash.utils.factory.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PracticeService extends BaseService<Practice, Long, PracticeRepository> {
    @Autowired
    private PracticeRepository practiceRepository;

    /**
     * 查询首页最近5条资讯数据
     *
     * @return
     */
    public List<Practice> queryIndexNews() {
        Page<Practice> page = query(1, 5, ChannelEnum.NEWS.getId());
        return page.getRecords();
    }

    public Page<Practice> query(int currentPage, int size, Long idChannel) {
        Page page = new Page(currentPage, size, "id");
        page.addFilter(SearchFilter.build("idChannel", SearchFilter.Operator.EQ, idChannel));
        return queryPage(page);

    }
}
