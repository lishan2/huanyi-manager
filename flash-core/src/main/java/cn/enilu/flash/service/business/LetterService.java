package cn.enilu.flash.service.business;

import cn.enilu.flash.bean.entity.business.Letter;
import cn.enilu.flash.bean.entity.enterprise.Cooperation;
import cn.enilu.flash.bean.enumeration.cms.ChannelEnum;
import cn.enilu.flash.bean.vo.query.SearchFilter;
import cn.enilu.flash.dao.business.LetterRepository;
import cn.enilu.flash.dao.enterprise.CooperationRepository;
import cn.enilu.flash.service.BaseService;
import cn.enilu.flash.utils.factory.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LetterService extends BaseService<Letter, Long, LetterRepository> {
    @Autowired
    private LetterRepository letterRepository;

    /**
     * 查询首页最近5条资讯数据
     *
     * @return
     */
    public List<Letter> queryIndexNews() {
        Page<Letter> page = query(1, 5, ChannelEnum.NEWS.getId());
        return page.getRecords();
    }

    public Page<Letter> query(int currentPage, int size, Long idChannel) {
        Page page = new Page(currentPage, size, "id");
        page.addFilter(SearchFilter.build("idChannel", SearchFilter.Operator.EQ, idChannel));
        return queryPage(page);

    }
}
