package cn.enilu.flash.bean.entity.business;

import cn.enilu.flash.bean.entity.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;

@Entity(name="t_business_letter")
@Table(appliesTo = "t_business_letter",comment = "私信我们")
@Data
@EntityListeners(AuditingEntityListener.class)
public class Letter extends BaseEntity {

    @Column(columnDefinition = "VARCHAR(255) COMMENT '内容'")
    private String content;
    @Column(columnDefinition = "VARCHAR(64) COMMENT '联系人'")
    private String person;
    @Column(columnDefinition = "VARCHAR(64) COMMENT '联系电话'")
    private String telephone;
    @Column(columnDefinition = "VARCHAR(64) COMMENT '标题'")
    private String title;

}
