package cn.enilu.flash.bean.entity.business;

import cn.enilu.flash.bean.entity.BaseEntity;
import lombok.Data;
import org.hibernate.annotations.Table;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.validation.constraints.NotBlank;

@Entity(name="t_business_image")
@Table(appliesTo = "t_business_image",comment = "轮播图管理")
@Data
@EntityListeners(AuditingEntityListener.class)
public class Image extends BaseEntity {
    @Column(columnDefinition = "VARCHAR(64) COMMENT '标题'")
    private String title;
    @Column(columnDefinition = "VARCHAR(64) COMMENT '文件ID'")
    private String idFile;
    @Column(columnDefinition = "bigint(10) COMMENT '状态'")
    private Long status;
    @Column(columnDefinition = "TEXT COMMENT '内容'")
    @NotBlank(message = "内容不能为空")
    private String content;
    @Column(columnDefinition = "VARCHAR(64) COMMENT '作者'")
    private String author;

}
