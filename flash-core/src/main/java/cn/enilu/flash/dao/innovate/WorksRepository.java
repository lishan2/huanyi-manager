
package cn.enilu.flash.dao.innovate;

import cn.enilu.flash.bean.entity.innovate.Works;
import cn.enilu.flash.dao.BaseRepository;

import java.util.List;

public interface WorksRepository extends BaseRepository<Works,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param idChannel
     * @return
     */
    List<Works> findAllByIdChannel(Long idChannel);
}
