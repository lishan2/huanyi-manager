
package cn.enilu.flash.dao.enterprise;

import cn.enilu.flash.bean.entity.enterprise.Affiche;
import cn.enilu.flash.dao.BaseRepository;
import java.util.List;

public interface AfficheRepository extends BaseRepository<Affiche,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param idChannel
     * @return
     */
    List<Affiche> findAllByIdChannel(Long idChannel);
}
