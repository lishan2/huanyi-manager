
package cn.enilu.flash.dao.enterprise;

import cn.enilu.flash.bean.entity.enterprise.ServicesTable;
import cn.enilu.flash.dao.BaseRepository;

import java.util.List;

public interface ServicesRepository extends BaseRepository<ServicesTable,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param idChannel
     * @return
     */
    List<ServicesTable> findAllByIdChannel(Long idChannel);
}
