
package cn.enilu.flash.dao.enterprise;

import cn.enilu.flash.bean.entity.enterprise.Cooperation;
import cn.enilu.flash.dao.BaseRepository;

import java.util.List;

public interface CooperationRepository extends BaseRepository<Cooperation,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param idChannel
     * @return
     */
    List<Cooperation> findAllByIdChannel(Long idChannel);
}
