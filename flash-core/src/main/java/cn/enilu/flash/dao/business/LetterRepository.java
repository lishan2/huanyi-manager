package cn.enilu.flash.dao.business;

import cn.enilu.flash.bean.entity.business.Letter;
import cn.enilu.flash.dao.BaseRepository;

public interface LetterRepository extends BaseRepository<Letter,Long> {

}
