
package cn.enilu.flash.dao.innovate;

import cn.enilu.flash.bean.entity.innovate.Education;
import cn.enilu.flash.dao.BaseRepository;

import java.util.List;

public interface EducationRepository extends BaseRepository<Education,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param idChannel
     * @return
     */
    List<Education> findAllByIdChannel(Long idChannel);
}
