
package cn.enilu.flash.dao.innovate;

import cn.enilu.flash.bean.entity.innovate.Practice;
import cn.enilu.flash.dao.BaseRepository;

import java.util.List;

public interface PracticeRepository extends BaseRepository<Practice,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param idChannel
     * @return
     */
    List<Practice> findAllByIdChannel(Long idChannel);
}
