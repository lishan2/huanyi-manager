
package cn.enilu.flash.dao.business;

import cn.enilu.flash.bean.entity.business.Image;
import cn.enilu.flash.dao.BaseRepository;

public interface ImageRepository extends BaseRepository<Image,Long> {
}
