
package cn.enilu.flash.dao.enterprise;

import cn.enilu.flash.bean.entity.enterprise.Flow;
import cn.enilu.flash.dao.BaseRepository;

import java.util.List;

public interface FlowRepository extends BaseRepository<Flow,Long> {
    /**
     * 查询指定栏目下所有文章列表
     * @param id
     * @return
     */
    List<Flow> findAllByIdChannel(Long id);

}
